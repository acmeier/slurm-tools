#!/bin/bash

# Examplehow to run "Destripe"

set -e

#====== Set Path to helper scripts ===========
gen_joblist_time="/home/ameier/code/slurm/slurm-tools/gen_joblist_time.sh"
slurm_submit_joblist="/home/ameier/code/slurm/slurm-tools/slurm_submit_joblist.sh"

#====== Set Parameters ===========

# Specify the parameterfile to use
EXT="v095"
parfile=$(realpath "./destripe_s5p_oclo_"${EXT}"_add_clouds_pv.par") 

# Set the start and end date of the computation
# Note that your array must be smaller than 1000 items. Maybe split into several date periods....
date_start="2020-01-01"
date_end="2021-03-31"


#====== Create joblist file ===========
# put the command in parentheses to make it an array
# This makes it easy to pass it as a single argument later and you do not have to take (so much) care on proper quoting
CMD_TEMPLATE=("/home/ameier/bin/Destripe.sh ${parfile} %START %END")

echo  "Template is: $CMD_TEMPLATE"

# specify the location of the jobfile
jobfile="./oclo_DestripePV_"${EXT}"_${date_start}_${date_end}.txt"

# use "${CMD_TEMPLATE[@]}" to pass as a single argument
# note that we use the "-g" switch to convert the date to the German date notation (dd.mm.yyyy). We also could have used -d "%d.%m.%Y".
"$gen_joblist_time" -g "${CMD_TEMPLATE[@]}" $date_start $date_end $jobfile


## Comment out the next line if you only want to create the joblist without actually submitting
# exit 1

#====== Submit as array job ===========
# This step creates a file in /home/${USER}/jobs/ that is submitted to the queue.
# That while that script is running, it reads from the file you specify in $joblist!
# So make sure you do not modify that file while your jobs are running!
joblist="$jobfile"

"$slurm_submit_joblist" \
                        -n 9 \  # specify number of simultanous runs
                        -j oclo_DestripePV \ # provde a basename for your jobname
                        -m 8500M \ # specify the memory you need for a single instance of your command
                        "$joblist" # Path to the joblist file