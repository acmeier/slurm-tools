#/bin/bash


set -e

#====== Set Path to helper scripts ===========
gen_joblist_time="/home/ameier/code/slurm/slurm-tools/gen_joblist_time.sh"
slurm_submit_joblist="/home/ameier/code/slurm/slurm-tools/slurm_submit_joblist.sh"

#====== Set Parameters ===========
EXTENSION="v09"
date_start="2018-04-20"
date_end="2018-12-31"

date_start="2019-01-01"
date_end="2019-12-31"

date_start="2020-01-01"
date_end="2021-01-31"

#====== Create joblist file ===========
# put the command in brackets to make it an array
# This makes it easy to pass it as a single argument later

CMD_TEMPLATE=("python /home/ameier/code/prj/oclo/bin/time_series_regional/cli_create_timeseries_v3.py \
	/oss/ameier/oclo/slant \
	$EXTENSION \
	/home/ameier/scripts/oclo/test_timeseries_janis/data \
	scd_oclo \
	%START %END \
	--sza_min 89 \
	--sza_max 91 \
	--lat_min -90 \
	--lat_max 90 \
	--lat_delta 90")

# --rms_max 1e20 \
# 	--intensity_max 1e20 \

echo  "Template is: $CMD_TEMPLATE"

jobfile="./timeseries_sza90_"${EXT}"_${date_start}.txt"

# use "${CMD_TEMPLATE[@]}" to pass as a single argument
"$gen_joblist_time" "${CMD_TEMPLATE[@]}" $date_start $date_end $jobfile
# exit 1
#====== Submit as array job ===========

joblist="$jobfile"

"$slurm_submit_joblist" \
                        -n 365 \
                        -j oclo_proc \
                        -m 8500M \
                        "$joblist"