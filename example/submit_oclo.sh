#!/bin/bash


CMD_TEMPLATE="/home/ameier/bin/slurm/oclo/processor_irrad_v1_ext.sh p_6_wls_345_wle_389_rs_mr6090_mrCleanWaterTropicsi %START %END"
# CMD_TEMPLATE='nlin ffff.par -d --START-- --END--'

date_start="2020-10-01"
date_end="2021-01-20"
outfile='./oclo_test.txt'

./gen_joblist_time.sh "$CMD_TEMPLATE" $date_start $date_end $outfile

####################

joblist="$outfile"

./slurm_submit_joblist.sh \
                        -n 800 \
                        -j oclo_proc \
                        -m 8500M \
                        "$joblist"