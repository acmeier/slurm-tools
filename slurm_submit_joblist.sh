#!/bin/bash
#set -x

#----------------
# Author: Andreas Meier (ameier@iup.physik.uni-bremen.de)
# Credits: This script is based on a script written by Mark Weber
#----------------

#---------------------------------------------------
# Read configuration from file or set here in the script
#---------------------------------------------------
ENV_CONFIG_FILE="/home/$USER/.my_env_config"
if [ -f "$ENV_CONFIG_FILE" ];then
    source "$ENV_CONFIG_FILE"
    LOGDIR="$MY_LOGDIR"
    JOBDIR="$MY_JOBDIR"
else
    $ENV_CONFIG_FILE="/dev/null"
    MY_EMAIL="${USER}@iup.physik.uni-bremen.de"
    MY_TEMPDIR="/home/${USER}/tmp"; mkdir -p "$MY_TEMPDIR"
    MY_LOGDIR="/home/${USER}/logs"; mkdir -p "$MY_LOGDIR"
    MY_JOBDIR="/home/${USER}/jobs"; mkdir -p "$MY_JOBDIR"
fi


#---------------------------------------------------
# Define help message
#---------------------------------------------------
usage() {
cat <<  EOF

Submit an array job to the cluster using sbatch.
The commands to execute are defined in a text file <Joblist> with one command per row.
The last command in <Joblist> must end with a newline character!
----------------------------------------------------------------
USAGE: ${0##*/} [OPTIONS] JOBLIST

Mandatory arguments:
JOBLIST             File with commands. Must have newline after last row.
	
Optional arguments:
-a <arrayarg>	: argument for "sbatch --array". See "man sbatch".
                  Use this when you only want to submit certain rows (e.g. failed jobs)
                  Example: -a 23,50,100
                  Note that <nactive> is ignored in this case!
                  If omitted all rows in JOBLIST are submitted.
-n <nactive>    : Number of simultaneous jobs.
-j <jobname>    : Name of the job. [JOBLISTNAME_JOBID_ARRAYID]
-m <memory>     : Memory requirement per job. [8500M]
-o <outfile>    : File to dump stdout. ["/home/${USER}/logs"/[...].out]
-e <errfile>    : File to dump stderr. ["/home/${USER}/logs"/[...].err]
EOF
}



#---------------------------------------------------
# Parse arguments
#---------------------------------------------------
MANUAL_ARRAYARG=0
OPTIND=1
while getopts ":a:n:j:m:o:e:h" opt; do
    case $opt in

    a) ARRAYARG="$OPTARG"
        MANUAL_ARRAYARG=1
        ;;
    n) NACTIVE="$OPTARG"
       ;;
    j) JOBNAME="$OPTARG"
        ;;
    m) MEM="$OPTARG"
    ;;
    o) OUTFILE="$OPTARG"
	    ;;
    e) ERRFILE="$OPTARG"
       ;;
	h | *) # Display help.
      usage
      exit 0
	;;
   \?) echo "Invalid option -$OPTARG" >&2
       exit 1
#    ;;
  esac
done
# Make  positional arguments accessible via $1, $2 ...
shift "$((OPTIND-1))"   # Discard the options and sentinel --
JOBFILE="$1"

# Quit if jobfile is not provided
if [ -z $JOBFILE ];then
	usage
	exit 1
fi


#---------------------------------------------------
# Set defaults
#---------------------------------------------------
# Store current date
SUBMIT_DATE=$(date +"%y%m%d_%H%M%S")

# Get the filename of the jobfile without extension
JOBFILE_STEM=$(basename ${JOBFILE}| rev | cut -f 2- -d '.' | rev )
# Get the number of lines (jobs) in the joblist
JOBCOUNT=$(cat $JOBFILE | wc -l)

# Set values for parameters not set as optional argument
if [ -z "$NACTIVE" ];then NACTIVE=500;fi
if [ -z "$MEM" ]    ;then MEM="8500M";fi
if [ -z "$JOBNAME" ];then JOBNAME=${JOBFILE_STEM}_${SUBMIT_DATE};fi
if [ -z "$OUTFILE" ];then OUTFILE="${LOGDIR}/${JOBFILE_STEM}'_%A-%a.out'"; fi
if [ -z "$ERRFILE" ];then ERRFILE="${LOGDIR}/${JOBFILE_STEM}'_%A-%a.err'"; fi
if [ -z "$ARRAYARG" ];then ARRAYARG="1-${JOBCOUNT}%$NACTIVE";fi

echo "NACTIVE: $NACTIVE"
echo "MEM: $MEM"
echo "JOBNAME: $JOBNAME"
echo "OUTFILE: $OUTFILE"
echo "ERRFILE: $ERRFILE"
echo "JOBFILE_STEM: $JOBFILE_STEM"


#---------------------------------------------------
# # Create a script to submit to the cluster
#---------------------------------------------------
JOBSH="${JOBDIR}/${JOBFILE_STEM}_${SUBMIT_DATE}.sh"

if [ -f $JOBSH ];then rm -rf $JOBSH;fi
echo '#!/bin/bash' >> $JOBSH
echo "#SBATCH --job-name=${JOBNAME}" >> $JOBSH
echo "#SBATCH --mem=${MEM}" >> $JOBSH
# echo "#SBATCH --array=1-${JOBCOUNT}%$NACTIVE" >> $JOBSH
echo "#SBATCH --array=$ARRAYARG" >> $JOBSH
echo "#SBATCH --output=${OUTFILE}" >> $JOBSH
echo "#SBATCH --error=${ERRFILE}">> $JOBSH
echo "source $ENV_CONFIG_FILE" >> $JOBSH
echo "JOBFILE=$(realpath $JOBFILE)" >> $JOBSH
echo 'cmd=$(head -n "$SLURM_ARRAY_TASK_ID" "$JOBFILE" | tail -n 1 )' >> $JOBSH # note that we use single quotation mark to write the literal string
echo '$cmd' >> $JOBSH # note that we use single quotation mark to write the literal string
chmod u+x  $JOBSH
echo $JOBSH

#---------------------------------------------------
# Submit the job
#---------------------------------------------------
sbatch $JOBSH

echo -e "Submitted $JOBFILE to the queue\n"
if [ "$MANUAL_ARRAYARG" -eq 0 ]; then
    echo -e "With $JOBCOUNT items"
fi

