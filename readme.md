# Slurm-tools

This resoitory contains some bash scripts to run commands on a SLURM cluster.

The `dotfiles` folder contains:
* `.bashrc` An example with aliases for some SLURM commands.
* `.my_env_config`A file to set the environment available on the computing nodes. Use it for `module load xxx` or to set the Python `conda` environment.

These files are expected in your home directory (`/home/${USER}/`)


## Suggested workflow

Jobs with the same (or similar) requirements can be submitted as "array jobs". This is generally the case if you run your program for each date in a timeseries.  Array jobs recommended way to submit many jobs [^1] .

If you use the scripts below, the submission of an array job is as follows:

1. Create a text file `joblist.txt` with one command per row.  Use `gen_jobfile_time.sh` or any program you like.
2. Submit the commands listed in `jobfile.txt`  jobs to the cluster using `slurm_submit_joblist.sh` 

An example script to define your commands and subsequent submission to the cluster looks as follows. 

````bash
#!/bin/bash


#--------------------------
# Generate the joblist file
#---------------------------
CMD_TEMPLATE="/home/ameier/bin/slurm/oclo/processor_irrad_v1_ext.sh p_6_wls_345_wle_389 %START %END"

date_start="2020-10-01"
date_end="2021-01-20"
outfile='./oclo_test.txt'

./gen_joblist_time.sh "$CMD_TEMPLATE" $date_start $date_end $outfile

#=======================================

#--------------------------
# Submit the joblist file
#---------------------------

joblist="$outfile"

./slurm_submit_joblist.sh \
                        -n 500 \
                        -j oclo_proc \
                        -m 8500M \
                        "$joblist"
````



### Creating the joblist

#### Using `gen_jobfile_time.sh`

The idea of this script is, that you provide a template of you command with placeholders for the date parameters (specifies as `%START` and `%END`). The date parameters are then substituted based on the start date `START`, stop date `STOP` and the time step increment `FREQ`. The default time step is 1 day.

The script has required and optional parameters:

`````bash
 ./gen_joblist_time.sh -h

Generate a file with a list of commands based on CMD_TEMPLATE with date arguments corresponding to START END and FREQ

Usage: ${0##*/} [-hg [-f <FREQ> -d <DATEFORMAT>] CMD_TEMPLATE START END OUTFILE

Positional arguments:
CMD_TEMPLATE    : A qoted string of your command in which the dates are substituted by %START and %END, respectively
                    Example:
                    'nlin_c config.par -d %START %END -t'

START           : Start date in iso format (yyyy-mm-dd)
END             : Start date in iso format (yyyy-mm-dd)
OUTFILE         : File path to the output (e.g. /home/ameier/jobs/myjob.txt) 

Optional arguments:
-h                  : display this help and exit
-d <DATEFORMAT>  : Format string for the date (e.g. %d.%m.%Y or %y%m%d)
-g                  : Shorthand German date format (%d.%m.%Y)  
-f <FREQ>        : Frequency or time step for the time increment. Default: 1day . (e.g. 3months) 
`````



An example for `NLIN` is shown below. Note that we use `/dev/stdout` as `OUTFILE` in order to print the output to the console.

````bash
./gen_joblist_time.sh -d %d.%m.%Y -f 1day 'nlin_c config.par -d %START %END -t' 2020-01-01 2020-01-02 /dev/stdout
nlin_c config.par -d 01.01.2020 01.01.2020 -t
nlin_c config.par -d 02.01.2020 02.01.2020 -t
````

Here is an example to create monthly timesteps using the `-f 1month` switch:

````bash
./gen_joblist_time.sh -d %d.%m.%Y -f 1month 'nlin_c config.par -d %START %END -t' 2020-01-01 2020-12-31 /dev/stdout
nlin_c config.par -d 01.01.2020 31.01.2020 -t
nlin_c config.par -d 01.02.2020 29.02.2020 -t
nlin_c config.par -d 01.03.2020 31.03.2020 -t
nlin_c config.par -d 01.04.2020 30.04.2020 -t
nlin_c config.par -d 01.05.2020 31.05.2020 -t
nlin_c config.par -d 01.06.2020 30.06.2020 -t
nlin_c config.par -d 01.07.2020 31.07.2020 -t
nlin_c config.par -d 01.08.2020 31.08.2020 -t
nlin_c config.par -d 01.09.2020 30.09.2020 -t
nlin_c config.par -d 01.10.2020 31.10.2020 -t
nlin_c config.par -d 01.11.2020 30.11.2020 -t
nlin_c config.par -d 01.12.2020 31.12.2020 -t
````



Of course you can also create the list of commands by other means (e.g from python). Here are a few things to consider when creating these files.

**Requirements for ``joblist.txt`**:

* use `\n` as newline character (Unix style). Not sure if `\r\n` also works.
* The last line must be empty (`\n` after the last character of your last command)
* Use an extension (any you like), or do not use dots `.` in your filename
* Better do not use whitespace in the filenames



### Submit the joblist

To submit the joblist file to the cluster use the script `slurm_submit_joblist.sh` You should define the requirements of your jobs using the optional parameters. The options are listed below:

````bash
./slurm_submit_joblist.sh 

Submit an array job to the cluster using sbatch.
The commands to execute are defined in a text file <Joblist> with one command per row.
The last command in <Joblist> must end with a newline character!
----------------------------------------------------------------
USAGE: ${0##*/} [OPTIONS] JOBLIST

Mandatory arguments:
JOBLIST             File with commands. Must have newline after last row.
	
Optional arguments:
-a <arrayarg>	: argument for "sbatch --array". See "man sbatch".
                  Use this when you only want to submit certain rows (e.g. failed jobs)
                  Example: -a 23,50,100
                  Note that <nactive> is ignored in this case!
                  If omitted all rows in JOBLIST are submitted.
-n <nactive>    : Number of simultaneous jobs.
-j <jobname>    : Name of the job. [JOBLISTNAME_JOBID_ARRAYID]
-m <memory>     : Memory requirement per job. [8500M]
-o <outfile>    : File to dump stdout. ["/home/${USER}/logs"/[...].out]
-e <errfile>    : File to dump stderr. ["/home/${USER}/logs"/[...].err]
````



### Full example script to submit a job

This example shows how to define and submit a job using a script for your task using the helper scripts explained above.

We specify:

* a maximum of 500 jobs in parallel (`-n 500`)
*  set the name of the job to "oclo_proc" (`-j oclo_proc`)
* reserve 8.5 GB of memory for each of the jobs (`-m 8500M`)

````bash
#!/bin/bash


CMD_TEMPLATE="/home/ameier/bin/slurm/oclo/processor_irrad_v1_ext.sh p_6_wls_345_wle_389 %START %END"

date_start="2020-10-01"
date_end="2021-01-20"
outfile='./oclo_test.txt'

./gen_joblist_time.sh "$CMD_TEMPLATE" $date_start $date_end $outfile

####################

joblist="$outfile"

./slurm_submit_joblist.sh \
                        -n 500 \
                        -j oclo_proc \
                        -m 8500M \
                        "$joblist"
````

Further examples can be found in the `examples` directory.

## Alternatives

There are some alternatives for job submission using a joblist.

### Dead simple Queue

A python CLI utility for the job submission developed at Yale Center for Research Computing. See 
* https://docs.ycrc.yale.edu/clusters-at-yale/job-scheduling/dsq/
* https://github.com/ycrc/dSQ

It is quite advanced and also lets you resubmit failed jobs. However, that would require installation by the system administrators.

### A Solution in Perl
* https://github.com/molecules/Perl6-misc/blob/master/sbatch_run



[^1]: https://slurm.schedmd.com/quickstart.html
