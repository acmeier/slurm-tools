#!/bin/bash

#----------------
# Author: Andreas Meier (ameier@iup.physik.uni-bremen.de)
#----------------


# Getops doc
# https://wiki.bash-hackers.org/howto/getopts_tutorial
# http://mywiki.wooledge.org/BashFAQ/035


# Define helper functions
# ==========================================
iso2epoch(){
    # Convert iso date (yyy-mm-dd) to epoch (seconds since 1970)
    date +%s -u -d "$1"
}

iso2fmt(){
    # Convert iso date to derired format specifies in $2, e.g %d.%m.%Y
    date +"$2" -u -d "$1"
}

# Define help message
# ==========================================
usage() {
cat << EOF

Generate a file with a list of commands based on CMD_TEMPLATE with date arguments corresponding to START END and FREQ

Usage: ${0##*/} [-hg [-f <FREQ> -d <DATEFORMAT>] CMD_TEMPLATE START END OUTFILE

Positional arguments:
CMD_TEMPLATE    : A qoted string of your command in which the dates are substituted by %START and %END, respectively
                    Example:
                    'nlin_c config.par -d %START %END -t'

START           : Start date in iso format (yyyy-mm-dd)
END             : Start date in iso format (yyyy-mm-dd)
OUTFILE         : File path to the output (e.g. /home/ameier/jobs/myjob.txt) 

Optional arguments:
-h                  : display this help and exit
-d <DATEFORMAT>  : Format string for the date (e.g. %d.%m.%Y or %y%m%d)
-g                  : Shorthand German date format (%d.%m.%Y)  
-f <FREQ>        : Frequency or time step for the time increment. Default: 1day . (e.g. 3months) 
EOF
}


# Parse optional parameters
# ==========================================

OPTIND=1
# Resetting OPTIND is necessary if getopts was used previously in the script.
# It is a good idea to make OPTIND local if you process options in a function.

while getopts :hgf:d: opt; do
    case $opt in
        h)
            usage
            exit 0
            ;;
        g)  FMT="%d.%m.%Y"
            ;;
        f)  FREQ=$OPTARG
            ;;
        d)  FMT=$OPTARG
            ;;
        *)
            usage >&2
            exit 1
            ;;
    esac
done
# Make  positional arguments accessible via $1, $2 ...
shift "$((OPTIND-1))"   # Discard the options and sentinel --


if [ "$#" -ne 4 ]; then
    echo "Illegal number of parameters"
    echo "you passed: $@"
    echo "This was interpreted as "$#" parameters"
    usage
    exit 1
fi


# Set defaults
# ==========================================
CMD_TEMP="$1"
START="$2"
END="$3"
OUTFILE="$4"
if [ -z "$FREQ" ]; then FREQ='1 days'; fi
if [ -z "$FMT" ]; then FMT="%Y-%m-%d"; fi



# Time loop to write command list to a file
# ==========================================
# remove file if it exists
if [ -f "$OUTFILE" ]; then
    rm "$OUTFILE"
fi


T1=$START
T2=$END
CNT=0
while [ $(iso2epoch "$T1") -le $(iso2epoch "$END") ] 
do

    # count the number of submitted jobs
    let CNT=$CNT+1
  
    T2=$(date "+%Y-%m-%d" -u -d "$T1 + ${FREQ} - 1 day")
  
  # Convert iso-date to desired output format 
    T1STR=$(iso2fmt $T1 $FMT)
    T2STR=$(iso2fmt $T2 $FMT)
    
    # Replace placeholders with actual date
    CMD=$(echo "$CMD_TEMP" | sed -e "s/%START/"$T1STR"/g" | sed -e "s/%END/"$T2STR"/g") 
  
    # write command to file
    echo "$CMD" >> "$OUTFILE"
    #echo $CMD

    #increment time step
    T1=$(date "+%Y-%m-%d" -u -d "$T1 +${FREQ}")
done