# Sample .bashrc for SuSE Linux
# Copyright (c) SuSE GmbH Nuernberg

# There are 3 different types of shells in bash: the login shell, normal shell
# and interactive shell. Login shells read ~/.profile and interactive shells
# read ~/.bashrc; in our setup, /etc/profile sources ~/.bashrc - thus all
# settings made here will also take effect in a login shell.
#
# NOTE: It is recommended to make language settings in ~/.profile rather than
# here, since multilingual X sessions would not work properly if LANG is over-
# ridden in every subshell.

# Some applications read the EDITOR variable to determine your favourite text
# editor. So uncomment the line below and enter the editor of your choice :-)
#export EDITOR=/usr/bin/vim
#export EDITOR=/usr/bin/mcedit

# For some news readers it makes sense to specify the NEWSSERVER variable here
#export NEWSSERVER=your.news.server

# If you want to use a Palm device with Linux, uncomment the two lines below.
# For some (older) Palm Pilots, you might need to set a lower baud rate
# e.g. 57600 or 38400; lowest is 9600 (very slow!)
#
#export PILOTPORT=/dev/pilot
#export PILOTRATE=115200

test -s ~/.alias && . ~/.alias || true

# ==================================
# ------- ADDED BY MYSELF ----------
# ==================================

# use US language settings
export LC_ALL="en_US.UTF-8"
export LANG="en_US.UTF-8"
export LANGUAGE="en_US.UTF-8"

# # set up module system for EasyBuild
# # ==================================
# # activate module system
# source /etc/profile.d/modules.sh
# # enable EasyBuild modules
# module use /oss/easybuild/modules/all
# module use /oss/easybuild/DOAS/modules/all
# # disable all other modules
# module unuse /usr/share/modules
# module unuse /usr/share/Modules/$MODULE_VERSION/modulefiles
# module unuse /opts/modulefiles
# module use /oss/easybuild/DOAS/modules/all/NLIN_C
# module load NLIN_C



# make all new files readable for everyone
umask 0022

# # qstat: print full jobname
# # https://stackoverflow.com/questions/26104116/qstat-and-long-job-names
# function qstat_long() {
# qstat -xml "$@" | tr "\n" " " | sed "s#<job_list[^>]*>#\n#g" | sed "s#<[^>]*>##g" | grep "[[:space:]]" | column -t
# }


# add anaconda  PYTHON to the path
#==============================
# PATH=/opts/anaconda3/bin:$PATH
# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/home/ameier/anaconda3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/home/ameier/anaconda3/etc/profile.d/conda.sh" ]; then
        . "/home/ameier/anaconda3/etc/profile.d/conda.sh"
    else
        export PATH="/home/ameier/anaconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<


#=== define alias to launch jupyter lab'===
alias jlremote='jupyter lab --no-browser --port=9127'


#----------------
# SLURM aliases
#-------------------
alias mysqueue='squeue --format="%.8i %.9P %30j %10u %.8T %.12M %9N"'
alias mysinfo='sinfo -N --format "%.10n %.9P %.15C %.10m %.10O %.8t"'